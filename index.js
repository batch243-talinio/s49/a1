//console.log("Zawarudo!");


// fetch()
	// this is a method in JavaScript that is used to send request in the server and load the information (response from the server) in the webages.

	// syntax:
		// fetch("urlAPI", {options/request from user and response to user})

	// API: https://jsonplaceholder.typicode.com/posts

	let fetchPosts = () => {
		fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json())
			.then(data => {
				//console.log(data)
				showPosts(data)
			})
	}

	fetchPosts();

	const showPosts = (posts) => {
		let postEntries = '';

		posts.forEach((post) => {
			postEntries += `
				<div id = "post-${post.id}">
					<h3 id = "post-title-${post.id}">${post.title}</h3>
					<p id = "post-body-${post.id}">${post.body}</p>
					<button onclick = "editPost('${post.id}')">Edit</button>
					<button onclick = "deletePost('${post.id}')">Delete</button>

				</div>
			`
		})
		document.querySelector("#div-post-entries").innerHTML = postEntries;
	}


// Add Post

	document.querySelector("#form-add-post").addEventListener("submit", (event) => {
		event.preventDefault();

		let title = document.querySelector("#txt-title").value;
		let body = document.querySelector("#txt-body").value;

		// if(!title.value){
		// 	document.querySelector("#emptyTitle").value = "Please input Title";
		//     return console.log("Please input Title");

		// }else if(!body.value){
		// 	document.querySelector("#emptyBody").value = "Please input Body";
		// 	return console.log("Please input Body");
		// }

		fetch("https://jsonplaceholder.typicode.com/posts", {
			method: "POST",
			body: JSON.stringify({
				title: title,
				body: body,
				userId: 1
			}),
			headers: {
				"Content-Type": "application/json"
			}
		}).then(response => response.json()).then(data => {
				console.log(data);
				alert("Success!");

				document.querySelector("#txt-title").value = null;
				document.querySelector("#txt-body").value = null;

				//fetchPosts()
		});
	})



// Edit posts
	const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector(`#txt-edit-title`).value = title;
		document.querySelector(`#txt-edit-body`).value = body;
		document.querySelector(`#txt-edit-id`).value = id;
		//.removeAttribute will remove the attribute from the selected element
		document.querySelector(`#btn-submit-update`).removeAttribute('disabled')

	}

	document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
		event.preventDefault();

		const id = document.querySelector("#txt-edit-id").value;
		const title = document.querySelector("#txt-edit-title").value;
		const body = document.querySelector("#txt-edit-body").value;

		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: "PATCH",
			body: JSON.stringify({
				title: title,
				body: body,
			}),
			headers: {
				"Content-Type": "application/json"
			}
		}).then(response => response.json()).then(data => {
			console.log(data)
			alert(`Successfully updated!`);

			document.querySelector("#txt-edit-id").value = null;
			document.querySelector("#txt-edit-title").value = null;
			document.querySelector("#txt-edit-body").value = null;

			document.querySelector(`#btn-submit-update`).setAttribute('disabled', true)

		})
	})


// Delete Post
	const deletePost = (id) => {
		/*console.log(`Hello from delete function!`)*/

		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: "DELETE"})
			.then(success => alert(`Post deleted successfully!`))

		document.querySelector(`#post-${id}`).remove();
	}









	// good/valid CODE

	// let specificPost = (id) => {
	// 	if(id > 100 || id < 1) { return console.log("Invalid!") }

	// 	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
	// 	.then(response => response.json())
	// 	.then(data => console.log(data))
	
	// }
	// specificPost(15);

